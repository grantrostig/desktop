'use strict';

const log = require('loglevel');
const Promise = require('bluebird');

function fetchAllCookies(win) {
  log.trace('fetchAllCookies()');
  return new Promise(function (resolve) {
    win.cookies.getAll({}, function (cookies) {
      resolve(cookies);
    });
  });
}

function deleteCookie(win, cookie) {
  return new Promise(function (resolve, reject) {
    const cookieUrl = `http${cookie.secure ? 's' : ''}://${cookie.domain}${cookie.path}`;

    win.cookies.remove({ url: cookieUrl, name: cookie.name }, function (result) {
      if (result) {
        log.info(`cookie removed:${result.name}`);
        resolve(result);
      } else {
        log.error('failed to remove cookie');
        reject(new Error('Failed to delete cookie.'));
      }
    });
  });
}

function flushCookies(win) {
  return new Promise(function (resolve) {
    fetchAllCookies(win)
      .then(function (cookies) {
        log.debug(`got ${cookies.length} cookies`);
        return Promise.all(
          cookies.map((cookie) => {
            return deleteCookie(win, cookie);
          }),
        );
      })
      .then(function () {
        log.info('deleted all cookies');
        resolve(true);
        return null;
      })
      .catch(function (err) {
        log.error(err.stack);
      });
  });
}

module.exports = flushCookies;
